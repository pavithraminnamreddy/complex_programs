package com.pavithra;

class Worker {
	String name;
	int age;
	float marks;

	Worker() {
	name="pavithra m";
	age=24;
	marks=99.0f;
	}

	Worker(String name, int age, float marks) {
		this.name = name;
		this.age = age;
		this.marks = marks;
	}

	void display() {
		System.out.println("name=" + name);
		;
		System.out.println("age=" + age);
		System.out.println("marks=" + marks);
	}
}

public class ParameterisedConstructor2 {
	public static void main(String[] args) {
		Worker w1 = new Worker("pavithra", 23, 98.0f);
		w1.display();
		Worker w2 = new Worker();
		w2.display();
	}
}
