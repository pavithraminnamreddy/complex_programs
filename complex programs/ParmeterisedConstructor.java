package com.pavithra;

class Employee {
	String name;
	int age;
	float marks;

	Employee(String name, int age, float marks) {
		this.name = name;
		this.age = age;
		this.marks = marks;
	}

	void display() {
		System.out.println("name=" + name);
		System.out.println("age=" + age);
		System.out.println("marks=" + marks);
	}
}

public class ParmeterisedConstructor {
	public static void main(String[] args) {
		Employee e1 = new Employee("pavithra", 24, 98.0f);
		e1.display();
		Employee e2 = new Employee("sam", 32, 99.0f);
		e2.display();
	}
}
